import 'package:flutter/material.dart';
import 'package:arrow_path/arrow_path.dart';

typedef GraphFunctionX = double Function(double x);
typedef GraphFunctionY = double Function(double y);

class Graph extends StatefulWidget {

  final List<GraphFunctionX> f_X;
  final List<GraphFunctionY> f_Y;

  @override
  State<StatefulWidget> createState() => _GraphState();

  Graph({
    this.f_X,
    this.f_Y
  });
}

class _GraphState extends State<Graph> {


  @override
  Widget build(BuildContext context) {

    return Container(
      height: 500,
      width: MediaQuery.of(context).size.width,
      child: CustomPaint(
        painter: _GraphPainter(
            widget.f_X,
            widget.f_Y
        ),
      ),
    );
  }
}

class _GraphPainter extends CustomPainter {

  final List<GraphFunctionX> functionsX;
  final List<GraphFunctionY> functionsY;

  _GraphPainter(this.functionsX, this.functionsY);

  @override
  void paint(Canvas canvas, Size size) {
    double leftPadding = 20;
    double topPadding = 20;
    var paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2.0
      ..color = Colors.black;
    var dX = Path()
      ..moveTo(leftPadding, size.height)
      ..lineTo(leftPadding, 0);
    var dY = Path()
      ..moveTo(0, size.height - topPadding)
      ..lineTo(size.width, size.height - topPadding);
    canvas.drawPath(ArrowPath.make(path: dX), paint);
    canvas.drawPath(ArrowPath.make(path: dY), paint);
    paint..color = Colors.red;
    this.functionsX.forEach((f) {
      var polygon = <Offset>[];
      for(double x = 0; x < size.width; x++) {

        polygon.add(Offset(
            x + leftPadding, (size.height - topPadding - f(x))
        ));
        print(f(x));
      }
      canvas.drawPath(Path()..addPolygon(polygon, false), paint);
    });
    this.functionsY.forEach((f) {
      var polygon = <Offset>[];
      for(double y = size.height - topPadding; y >= 0; y--) {
        polygon.add(Offset(
            leftPadding + f(y), y
        ));
      }
      canvas.drawPath(Path()..addPolygon(polygon, false), paint);
    });

  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;

}