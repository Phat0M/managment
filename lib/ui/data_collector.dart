import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inventorymanagmentsystem/busines_logic/ims.dart';
import 'package:inventorymanagmentsystem/ui/graph_screen.dart';

class DataCollector extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _StateDataCollector();

}

class _StateDataCollector extends State<DataCollector> {

  final List<DataField> dataFields = [];
  final List<String> data = [];

  @override
  void initState() {
    super.initState();
    dataFields.addAll([
      DataField(
        hint: 'Потребность (S), шт.',
        focus: FocusNode(),
        controller: TextEditingController(),
      ),
      DataField(
        hint: 'Опитмальный размер заказа (ОРЗ) ед.',
        focus: FocusNode(),
        controller: TextEditingController(),
      ),
      DataField(
        hint: 'Время поставки (T_п), дни',
        focus: FocusNode(),
        controller: TextEditingController(),
      ),
      DataField(
        hint: 'Время задержки поставки (Т_зп), дни',
        focus: FocusNode(),
        controller: TextEditingController(),
      ),
      DataField(
        hint: 'Число рабочих дней в периоде (N), дни',
        focus: FocusNode(),
        controller: TextEditingController(),
        inputAction: TextInputAction.done,
      ),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: SizedBox(
          child: Text(
            'Система управления запасами с фиксированным размером заказа',
            textAlign: TextAlign.center,
            maxLines: 4,
          ),
        ),
      ),
      body: bodyBuild(context),

    );
  }

  Widget bodyBuild(BuildContext context) {
    var fields = ListView.separated(
        itemBuilder: itemBuild,
        separatorBuilder: separatorBuild,
        itemCount: dataFields.length
    );

    return Padding(
      child: fields,
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
    );
  }

  Widget itemBuild (BuildContext context, int i) {
    return Padding(
      padding: EdgeInsets.all(3),
      child: TextFormField(
        controller: dataFields[i].controller,
        keyboardType: dataFields[i].keyboardType,
        focusNode: dataFields[i].focus,
        textInputAction: dataFields[i].inputAction,
        autovalidate: true,
        onFieldSubmitted: (String value) {
          dataFields[i].focus.unfocus();
          data.insert(i, value);
          print(dataFields.length);
          print(i);
          if(i < dataFields.length - 1) {
            FocusScope.of(context).requestFocus(dataFields[i + 1].focus);
          }
          else {
            var i = 0;
            var system = FixedOrderSize(
              double.parse(data[i++]),
              double.parse(data[i++]),
              int.parse(data[i++]),
              int.parse(data[i++]),
              int.parse(data[i++]),
            );
            Navigator.of(context).push(
                MaterialPageRoute(builder: (_) => GraphScreen(system: system,))
            );
          }
        },
        validator: validate,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 20),
          labelText: dataFields[i].hint,
          labelStyle: TextStyle(
            fontSize: 13.0
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(9999),
            borderSide: BorderSide(
              color: Colors.grey,
              width: 2.0
            )
          ),
        ),
      ),
    );
  }

  String validate(String data) {
    if(data != "" && double.tryParse(data) != null) {
      if(double.tryParse(data) < 0) {
        return "Значение должно быть >= 0";
      }
      return null;
    }
    return "Не должно быть пустым";
  }

  Widget separatorBuild (BuildContext context, int index) {
    return SizedBox(height: 15,);
  }

}

class DataField {
  final String hint;
  final FocusNode focus;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final TextInputAction inputAction;

  DataField({
    this.hint,
    this.focus,
    this.controller,
    this.keyboardType = TextInputType.number,
    this.inputAction = TextInputAction.next,
  });

}