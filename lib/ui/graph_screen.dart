import 'package:flutter/material.dart';
import 'package:inventorymanagmentsystem/busines_logic/ims.dart';

class GraphScreen extends StatefulWidget {

  final InventoryManagementSystem system;

  const GraphScreen({Key key, this.system}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _GraphScreenState();
}

class _GraphScreenState extends State<GraphScreen> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('График'),
      ),
      body: widget.system.graph,
    );
  }
}