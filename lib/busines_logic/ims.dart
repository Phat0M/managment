import 'dart:math';

import 'package:inventorymanagmentsystem/ui/graph_draw.dart';

abstract class InventoryManagementSystem {
  //потребность
  final double need;
  //время поставки
  final int time;
  //время задержки поставки
  final int delayTime;
  //количество дней
  final int daysCount;
  //дневное потребление
  double dailyIntake;
  //потребление за поставку
  double consumptionSupplies;
  //гарантийный запас
  double guaranteeStock;
  //максимальный запас
  double maxMargin;
  //оптимальный размер поставки
  final double optimalOrderSize;

  InventoryManagementSystem({
    this.need,
    this.time,
    this.delayTime,
    this.optimalOrderSize,
    this.daysCount,
  }) {
    dailyIntake = need / daysCount;
    guaranteeStock = dailyIntake * delayTime;
    consumptionSupplies = dailyIntake * time;
    maxMargin = optimalOrderSize + guaranteeStock;
  }

  Graph get graph;

}

class FixedOrderSize extends InventoryManagementSystem {

  //пороговый уровень запаса
  double thresholdStockLevel;
  //срок расходования запасов
  double termUseStocks;
  //срок расходования запасов до порогового уровня
  int termUseStocksToThreshold;

  FixedOrderSize(
      double need,
      double optimalOrderSize,
      int time,
      int delayTime,
      int daysCount,
      ) : super(
    need: need,
    optimalOrderSize:optimalOrderSize,
    time: time,
    delayTime: delayTime,
    daysCount: daysCount,
  ) {
    termUseStocks = optimalOrderSize / dailyIntake;
    thresholdStockLevel = guaranteeStock + consumptionSupplies;
  }

  static int countA = 0;
  @override
  Graph get graph {
    return Graph(
      f_X: [
          (x) => guaranteeStock,
          (x) => thresholdStockLevel,
          (x) => maxMargin,
            (x) {
              if (((-x + maxMargin) + (countA * optimalOrderSize)) - guaranteeStock < 0.0001) {
                if (countA != 1) {
                  print('incr');
                  countA ++;
                }
              }
              if(((-x + maxMargin) + (countA) * optimalOrderSize).round() <= 0) {
                countA ++;
              }
              print(countA);
              print(maxMargin);
              print('guar $guaranteeStock');
              print('value: ${(-x + maxMargin) + (countA) * optimalOrderSize}');
              return (-x + maxMargin) + (countA) * optimalOrderSize;
            }
      ],
      f_Y: [
      ],
    );
  }
}

class IntervalOrders extends InventoryManagementSystem {
  //пороговый уровень запаса
  double thresholdStockLevel;
  //срок расходования запасов
  double termUseStocks;
  //срок расходования запасов до порогового уровня
  int termUseStocksToThreshold;

  double interval;

  IntervalOrders(
      double need,
      double optimalOrderSize,
      int time,
      int delayTime,
      int daysCount,
      ) : super(
    need: need,
    optimalOrderSize:optimalOrderSize,
    time: time,
    delayTime: delayTime,
    daysCount: daysCount,
  ) {
    interval = daysCount / need / optimalOrderSize;
    termUseStocks = optimalOrderSize / dailyIntake;
    thresholdStockLevel = guaranteeStock + consumptionSupplies;
  }

  static int countA = 0;
  @override
  Graph get graph {
    return Graph(
      f_X: [
            (x) => guaranteeStock,
            (x) => thresholdStockLevel,
            (x) => maxMargin,
            (x) {
          if (x % interval == 0) {
            if (countA % 2 == 0) {
              print('incr');
              countA ++;
            }
          }
          if(((-x + maxMargin) + (countA) * optimalOrderSize).round() <= 0) {
            countA ++;
          }
          print(countA);
          print(maxMargin);
          print('guar $guaranteeStock');
          print('value: ${(-x + maxMargin) + (countA) * optimalOrderSize}');
          return (-x + maxMargin) + (countA) * optimalOrderSize;
        }
      ],
      f_Y: [
      ],
    );
  }
}